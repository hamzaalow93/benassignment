<?php


namespace App;

use Illuminate\Support\Facades\Http;


class Post
{
    private static $baseUrl= 'https://jsonplaceholder.typicode.com/posts';
    public static function all()
    {
        $response = Http::get(Post::$baseUrl);
        if ($response->successful()) {
            $CollectResponse = collect($response->json());
            return $CollectResponse->map(function ($item, $key) {
                return array(
                    "post_id"=>$item['id'],
                    "post_title"=>$item['title'],
                    "post_body"=>$item['body'],
                    "total_number_of_comments"=>Post::commentCount($item["id"]),
                );
            })->sortBy("total_number_of_comments")->values();
        } else {
            return $response->throw();
        }
    }
    public static function commentCount($postId){
        $response = Http::get(Post::$baseUrl."/$postId/comments");
        if ($response->successful()) {
            return count($response->json());
        } else {
            return 0;
        }
    }
}
