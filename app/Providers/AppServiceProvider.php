<?php

namespace App\Providers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('api', function ($status,$message,$data = [],$status_code = 200) {
            return Response::json([
                "status"=>$status,
                "message"=>$message,
                "data"=>$data
            ],$status_code);
        });
    }
}
