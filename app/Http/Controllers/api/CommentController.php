<?php

namespace App\Http\Controllers\api;

use App\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index(Request $request){
        return response()->api(true,"successful",Comment::all($request->only([
            "postId",
            "id",
            "name",
            "email",
            "body"
        ])));
    }
}
