<?php


namespace App;
use Illuminate\Support\Facades\Http;


class Comment
{
    private static $baseUrl= 'https://jsonplaceholder.typicode.com/comments';
    public static function all($query=[])
    {
        $response = Http::get(Comment::$baseUrl,$query);
        if ($response->successful()) {
            return $response->json();
        } else {
            return $response->throw();
        }
    }
}
